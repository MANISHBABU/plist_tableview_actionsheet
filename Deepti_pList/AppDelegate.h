//
//  AppDelegate.h
//  Deepti_pList
//
//  Created by Manish on 06/03/14.
//  Copyright (c) 2014 Manish@esense. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
