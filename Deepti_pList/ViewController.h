//
//  ViewController.h
//  Created by Manish on 29/07/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.


#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIActionSheetDelegate>{
    
    IBOutlet UITableView *userTableView;
    NSMutableArray *userNameArray;
    NSArray *usernameArrayMain;
    int buttonpresscheck;
    NSString *title ;
    
}

-(void)writeDataInArrayFromPlist;
-(void)writeDataInPlistFromArray;

//-(void)gestureSelfView;

@property(nonatomic,retain) NSMutableArray *userNameArray;
@property(nonatomic,retain) UITableView *userTableView;
@property (strong) IBOutlet UIView *tempView;
@property (strong) IBOutlet UITextField *customTextField;

-(IBAction)doneButtonPressed;
-(IBAction)showActionSheet;

@end
