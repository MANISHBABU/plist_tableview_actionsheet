
//  ViewController.m

//  Created by Manish Pathak on 29/07/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.


#import "ViewController.h"


@implementation ViewController
@synthesize userNameArray;
@synthesize userTableView;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self populateArray];
    [self writeDataInPlistFromArray];
   
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}




/// Reloading TableView in viewWillAppear so that the changes in TableView appears all the time.
-(void)viewWillAppear:(BOOL)animated
{
    
    [userTableView reloadData];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)userTableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([userNameArray count]>=1) {
        return[userNameArray count];
    }
    else{
        return 1;
    }
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    
    if(!cell){
               cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
             }
   
   /// Whatever is in UserNameArray it will populate in TableView Accordingly
    cell.textLabel.text=[userNameArray objectAtIndex:indexPath.row];

    return cell;
   
}




#pragma mark - Methods Implementaion.

-(IBAction)showActionSheet
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Save Value" delegate:self cancelButtonTitle:@"Cancel Button" destructiveButtonTitle:nil otherButtonTitles:@" 1 ", @" 2 ", @" 3 ", @" 4 ", @" 5 ",@"6",@"7", @"8",@"customTime",nil];
    
    actionSheet.actionSheetStyle = UIActionSheetStyleDefault; [actionSheet showInView:self.view];
    
}



/*---------------------------------------------------------------------------
 Checking Whether the Array is Null or not Before Giving it Memory  Condition
 is Implemented  Because When Very First time this application runs it should
 alloc memory to Mutable Array & after that there is no need of allocation
 ---------------------------------------------------------------------------*/

-(void)populateArray
{
    [self writeDataInArrayFromPlist];
    if ([userNameArray count]==0)
  {
        
    
    userNameArray = [[NSMutableArray alloc]init];
  }
    else
      {
        

    NSLog(@" Array Content %@",userNameArray);
      }
}




#pragma mark - pList Methods

/// This is to store data  to array from plist
-(void)writeDataInArrayFromPlist
{
    
    NSArray *directoryPath=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath=[directoryPath objectAtIndex:0];
    NSString *pListPath=[documentPath stringByAppendingPathComponent:@"my.plist"];
   
    userNameArray = [NSMutableArray arrayWithContentsOfFile:pListPath];
    
    [self.userTableView reloadData];
    
}

/// This is to store data  to pList from Array
-(void)writeDataInPlistFromArray
{
    
    NSFileManager *fileManager=[NSFileManager defaultManager];
    NSArray *directoryPath=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath=[directoryPath objectAtIndex:0];
    NSString *pListPath=[documentPath stringByAppendingPathComponent:@"my.plist"];
    BOOL success=[fileManager fileExistsAtPath:pListPath];
    NSError *error;
    NSLog(@" plist Path %@",pListPath);
    if(success && [userNameArray count]==0){
        userNameArray=[NSMutableArray arrayWithContentsOfFile:pListPath];
        NSLog(@" Array Content %@",userNameArray);
    }
    else{
        NSString *databasePath=[[[NSBundle mainBundle]resourcePath]stringByAppendingPathComponent:@"my.plist"];
        [fileManager copyItemAtPath:databasePath toPath:pListPath error:&error];
    }
    [userNameArray writeToFile:pListPath atomically:YES];
    [self writeDataInArrayFromPlist];
    [self.userTableView reloadData];
}




#pragma mark UIActionSheetDelegate

// Detecting Which Button is pressed in Action Sheet
// And Then adding the resepective data to USernameArray

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            title = [actionSheet buttonTitleAtIndex:buttonIndex];
            break;
        case 1:
            title = [actionSheet buttonTitleAtIndex:buttonIndex];
            break;
        case 2:
            title = [actionSheet buttonTitleAtIndex:buttonIndex];
            break;
        case 3:
            title = [actionSheet buttonTitleAtIndex:buttonIndex];
            break;
        case 4:
            title = [actionSheet buttonTitleAtIndex:buttonIndex];
            break;
        case 5:
            title = [actionSheet buttonTitleAtIndex:buttonIndex];
            break;
        case 6:
            title = [actionSheet buttonTitleAtIndex:buttonIndex];
            break;
        case 7:
            title = [actionSheet buttonTitleAtIndex:buttonIndex];
            break;
        case 8:
            NSLog(@"test");
            break;
    }
    if (buttonIndex == 8) {
        [self addFromView];
    }
    else{
         [userNameArray addObject:title];
    }
    
   /// Writing the changed userNameArray to pList
    [self writeDataInPlistFromArray];
    
}



// Adding Custom Time View to SubView of Main Xib
-(void)addFromView
{

    [self.view addSubview:_tempView];
   
}


// Adding the resepective data to USernameArray fromt the Text Field of TempView
-(IBAction)doneButtonPressed
{
    NSString *customTextFieldStringData = _customTextField.text;
    [userNameArray addObject:customTextFieldStringData];
    [self writeDataInPlistFromArray];
    [_tempView removeFromSuperview];
    [userTableView reloadData];
    
}

@end
